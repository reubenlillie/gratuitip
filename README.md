# GratuiTip<sup>™</sup>

[Use GratuiTip](https://gratuitip.netlify.com/) ([https://gratuitip.netlify.com/](https://gratuitip.netlify.com/)).

A state-based tip calculator written in vanilla JavaScript.

GratuiTip is built with the static site generator [Eleventy](https://11ty.io/) and hosted on [Netlify](https://netlify.com/).

The project is inspired by [Chris Ferdinandi](https://gomakethings.com/building-a-tip-calculator-with-vanilla-js/).

&copy; 2020 by [Reuben L. Lillie](https://github.com/reubenlillie/)
