---
title: GratuiTip™
subtitle: Tip Calculator
layout: layouts/base
inlineScripts:
  - 'gratuitip.js'
externalScripts:
  - 'component.min.js'
---
