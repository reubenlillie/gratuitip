/**
 * @file A helper function for creating state-based web components
 * @author Reuben L. Lillie
 * @see {@link https://vanillajsguides.com/web-apps/ Christ Ferdinandi}
 */
var Component = (function () {

  'use strict'

  /**
   * Define a constructor object for a new web component
   * @param {String} selector CSS selector for wrapping the component
   * @param {Object} options For state and templates
   * @see {@link https://vanillajstoolkit.com/boilerplates/constructor/ Christ Ferdinandi}
   */
  var Constructor = function (selector, options) {
    this.selector = selector
    this.state = options.state
    this.template = options.template
  }

  /**
   * Attach a function for rendering a template inside a component
   */
  Constructor.prototype.render = function () {
    var target = document.querySelector(this.selector)
    if(!target) return
    target.innerHTML = this.template(this.state)
  }

  /**
   * Attach a function for setting the current state of a component
   * @param {Object} obj To update the state
   */
  Constructor.prototype.setState = function (obj) {
    for (var key in obj) {
      if(obj.hasOwnProperty(key)) {
        this.state[key] = obj[key]
      }
    }
    this.render()
  }

  /**
   * Attach a function for getting the current state of a component
   */
  Constructor.prototype.getState = function () {
    return JSON.parse(JSON.stringify(this.state))
  }

  return Constructor

})()
