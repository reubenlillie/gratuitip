/**
 * @file A state-based tip calculator
 * @author Reuben L. Lillie
 * @summary Display tip totals inside an element with an id 'totals'
 */
(function () {

  'use strict'

  /**
   * A helper function to format a number as currency (USD$)
   * @param {Number} n An unformatted number
   * @return {String} The number in dollars and cents
   */
  var asCurrency = function (n) {
    return Number.parseFloat(n).toFixed(2)
  }

  /**
   * Create a component for displaying totals
   */
  var totals = new Component('#totals', {
    state: {},
    template: function (props) {
      // Calculate totals based on the current state
      var tipSubtotal = function () {
        return parseFloat(props.bill * (props.percentage / 100))
      }
      var total = function () {
        return asCurrency(props.bill + tipSubtotal())
      }
      var tipPerPerson = function () {
        return asCurrency(tipSubtotal() / props.payers)
      }
      var totalPerPerson = function () {
        return asCurrency(total() / props.payers)
      }
      // Display the proper template
      return (!props.bill  || !props.percentage || !props.payers)
        // Incomplete form
        ? '<p class="text-center">' +
            '<em>Enter a bill amount, tip percentage, and number of people paying.</em>' +
          '</p>'
        : props.payers === 1
          // Single payer
          ? '<p class="grid">' +
              '<span>Tip:</span>&nbsp;' +
              '<span class="monospace">$' +
                asCurrency(tipSubtotal()) +
              '</span>' +
            '<p class="grid">' +
                '<span>Total:</span>&nbsp;' +
                '<span class="monospace">$' + total() + '</span>' +
            '</p>'
          // Multiple payers
          : '<p class="grid">' +
              '<span>Total per person:</span>&nbsp;' +
              '<span class="monospace">$' +
                totalPerPerson() +
              '</span>' +
            '</p>' +
            '<details>' +
              '<summary>Breakdown</summary>' +
              '<p class="grid">' +
                '<span>Tip subtotal:</span>&nbsp;' +
                '<span class="monospace">$' +
                  asCurrency(tipSubtotal()) +
                '</span>' +
              '</p>' +
              '<p class="grid">' +
                '<span>Tip per person:</span>&nbsp;' +
                '<span class="monospace">$' +
                  tipPerPerson() +
                '</span>' +
              '</p>' +
              '<p class="grid">' +
                '<span>Total:</span>&nbsp;' +
                '<span class="monospace">$' +
                  total() +
                '</span>' +
              '</p>' +
            '</details>'
    }
  })

  /**
   * Handle form entries
   * @param {Object} event A input event
   */
  var inputHandler = function (event) {

    // Update state reactively
    totals.setState(Object.assign(totals.getState(), {
      bill: parseFloat(bill.value),
      percentage: parseFloat(percentage.value),
      payers: parseInt(people.value, 10)
    }))

    // Rerender the component
    totals.render()

  }

  // Initial render
  totals.render()

  // Event Listeners
  document.addEventListener('input', inputHandler,false)

 })()
