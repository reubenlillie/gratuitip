module.exports = function (data) {
  return `<!DOCTYPE html>
<html lang="en">
  <head>
    <title>${data.title} ${data.subtitle}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="180x180" href="/includes/assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/includes/assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/includes/assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="/includes/assets/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="/includes/assets/images/favicons/safari-pinned-tab.svg" color="#ff6347">
    <link rel="shortcut icon" href="/includes/assets/images/favicons/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="${data.title}">
    <meta name="application-name" content="${data.title}">
    <meta name="msapplication-TileColor" content="#ff6347">
    <meta name="msapplication-config" content="/includes/assets/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <meta property="og:title" content="${data.title} ${data.subtitle}">
    <meta property="og:description" content="${data.pkg.description}">
    <meta property="og:image" content="${data.pkg.url}/includes/assets/images/favicons/android-chrome-512x512.png">
    <meta property="og:url" content="${data.pkg.url}">
    <meta name="twitter:card" content="summary_large_image">
    <meta property="og:site_name" content="${data.title} ${data.subtitle}">
    <meta name="twitter:image:alt" content="The ${data.title} logo, a tomato-red colored letter G">
    <meta name="twitter:site" content="@${data.pkg.twitter}">
    <style>
      ${this.minifyCSS(this.fileToString('/includes/assets/css/inline.css'))}
    </style>
  </head>
  <body class="grid padding no-margin">
    <base target="_blank">
    <header class="grid-center-column text-center">
      <h1><span class="tomato">G</span>ratui<span class="tomato">T</span>ip<sup class="x-small">TM</sup></h1>
      <h2 class="monospace no-margin">${data.subtitle}</h2>
    </header>
    <main class="grid-center-column padding">
      <form autocomplete="off" class="flex flex-column">
        <label for="bill">Bill ($)</label>
        <input id="bill" name="bill" class="monospace padding" type="number" min="0" step="0.01">
        <label for="tip_percentage" value="20">Tip (%)</label>
        <input id="percentage" name="percentage" class="monospace padding" type="number" min="0">
        <label for="people">Number of People</label>
        <input id="people" name="people" class="monospace" type="number" min="1" value="1">
      </form>
      <div aria-live="polite" id="totals"></div>
    </main>
    <footer class="grid-center-column text-center x-small">
      <p class="no-margin">
        <span>&copy; 2020 by <a href="https://twitter.com/${data.pkg.author.twitter}">Reuben L. Lillie</a></span>
        &nbsp;|&nbsp;
        <span><a href="${data.pkg.repository.url}">View source</a></span>
      </p>
    </footer>
    ${data.externalScripts
      ? data.externalScripts.map(script => `<script src="/includes/assets/js/${script}"></script>`).join('')
      : ''
    }
    ${data.inlineScripts
      ? data.inlineScripts.map(script => `<script>
          ${this.minifyJS(this.fileToString(`/includes/assets/js/${script}`))}
        </script>`).join('')
      : ''
    }
  </body>
</html>`
}
