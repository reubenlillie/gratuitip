/**
 * @file Eleventy’s configuration file
 * @author Reuben L. Lillie
 * See {@link https://www.11ty.io/docs/config/ 11ty docs}
 */

// Require Node.js native fs module for interacting with the file system
var fs = require('fs')
// https://github.com/jakubpawlowicz/clean-css
var CleanCSS = require('clean-css')
// https://github.com/kangax/html-minifier
var htmlmin = require("html-minifier")
// https://github.com/terser-js/terser
var Terser = require('terser')

/** @module .eleventy */
module.exports = function (eleventyConfig) {

  /**
	 * Converts a file’s contents to a string.
	 * @param {string} file Path of the file to convert (relative to input)
	 * @return {string} Stringified version of target file’s contents
	 * @example	`${this.fileToString('includes/assets/css/inline.css')}`
	 */
	eleventyConfig.addFilter('fileToString', function (file) {
		return fs.readFileSync(`src${file}`).toString()
	})

  /**
   * Minify a stringified version of a Cascading Style Sheet
   * @param {String} stylesheet A raw stylesheet
   * @return {String} The minified stylesheet
   * @example	`${this.minifyCSS($this.fileToString('/includes/assets/css/inline.css'))}`
   * @see {@link https://github.com/jakubpawlowicz/clean-css#minify-method Package author README on GitHub}
   */
  eleventyConfig.addFilter("minifyCSS", function (stylesheet) {
    return minified = new CleanCSS({}).minify(stylesheet).styles
  })

  /**
   * Minify JavaScript
   * @param {String} code A JavaScript file’s contents
   * @return {String} The minified script
   * @example	`${this.minifyJS($this.fileToString('/includes/assets/js/gratuitip.js'))}`
   * See {@link https://www.11ty.io/docs/quicktips/inline-js/ 11ty docs}
   */
  eleventyConfig.addFilter("minifyJS", function(code) {
    var minified = Terser.minify(code)
    if(minified.error) {
      console.log("Terser error: ", minified.error)
      return code
    }

    return minified.code
  })

  /**
	 * Minifies HTML
	 * @param {string} content An HTML document
	 * @param {string} outputPath The path where 11ty outputs the HTML
	 * @return {string} The minified HTML
	 * @see {@link https://www.11ty.io/docs/config/#transforms 11ty docs}
	 */
	eleventyConfig.addTransform('minifyHTML', function(content, outputPath) {
		if( outputPath.endsWith('.html') ) {
			var minified = htmlmin.minify(content, {
				useShortDoctype: true,
				removeComments: true,
				collapseWhitespace: true
			})
			return minified
		}
		return content
	})

  /**
   * Copy static assets from the input directory to the output directory
   * See {@link https://www.11ty.io/docs/copy/ 11ty docs}
   */
  eleventyConfig.addPassthroughCopy('src/includes/assets/')

  /**
   * Return Eleventy’s optional Config object
   * See {@link https://www.11ty.io/docs/config/#using-the-configuration-api 11ty docs}
   */
  return {
    dir: {
      'includes': 'includes', // relative to input
      'input': 'src',
      'output': 'dist'
    }
  }

}
